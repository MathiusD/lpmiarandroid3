package iut.tutoriel3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnImg = findViewById(R.id.btnImg);
        btnImg.setOnClickListener((View v) ->  {
                ImageView img = findViewById(R.id.img);
                String imageURL = "https://images.punkapi.com/v2/89.png";
                Picasso.get().load(imageURL).into(img);
        });
        Button btnGet = findViewById(R.id.btnGet);
        btnGet.setOnClickListener((View v) -> {
            Ion.with(v.getContext ()).load("https://perdu.com")
            .asString()
            .setCallback(new FutureCallback<String >() {
                @Override
                public void onCompleted(Exception e, String  result) {
                    TextView tvResultat = findViewById(R.id.text);
                    tvResultat.setText(result );
                }
            });
        });
    }
}